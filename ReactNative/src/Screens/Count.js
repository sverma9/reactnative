import React,{useState} from 'react';
import { Button, Text } from 'react-native';

const Count = () =>{
    const [counter, setcounter] = useState(0)
    return (
        <>  
            <Text>Hello World</Text>
            <Text></Text>
            <Text></Text>
            <Text>Counter : {counter}</Text>
            <Text></Text>
            <Button 
                title='Increment' 
                onPress = {()=>{setcounter(counter + 1) } }/>
            <Text></Text>
            <Button title = 'Reset' onPress={ () => { setcounter(0) } }/>
        </>
    )
}

export default Count